package uz.optimal.retrofitpractise.business.mapper

import uz.optimal.retrofitpractise.business.data.UIPhoto
import uz.optimal.retrofitpractise.common.mapper.EntityMapper
import uz.optimal.retrofitpractise.network.data.model.DomainPhoto
import javax.inject.Inject

class PhotoEntityMapper @Inject constructor() :
    EntityMapper<DomainPhoto, UIPhoto> {
    override fun mapFromEntity(entity: DomainPhoto): UIPhoto {
        return UIPhoto(
            entity.id,
            entity.author,
            entity.width,
            entity.downloadUrl,
            entity.url,
            entity.height
        )
    }

    override fun mapToEntity(uiModel: UIPhoto): DomainPhoto {
        TODO("Not yet implemented")
    }

}