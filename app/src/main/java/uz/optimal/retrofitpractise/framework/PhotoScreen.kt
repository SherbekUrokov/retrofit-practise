package uz.optimal.retrofitpractise.framework

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.optimal.retrofitpractise.R
import uz.optimal.retrofitpractise.business.data.UIPhoto
import uz.optimal.retrofitpractise.common.UIState
import uz.optimal.retrofitpractise.databinding.ScreenPhotosBinding

@AndroidEntryPoint
class PhotoScreen :Fragment(R.layout.screen_photos){

    private val binding by viewBinding(ScreenPhotosBinding::bind)


    private val viewModel by viewModels<MainViewModel>()
    private var listPhoto: ArrayList<UIPhoto> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getPhotos()

        viewModel.photos.observe(viewLifecycleOwner, Observer {

            when(it){
                is UIState.Success ->{
                    listPhoto = it.data as ArrayList<UIPhoto>
                }
            }

        })

        binding.rv.adapter = MyRecAdapter(listPhoto)

    }

}